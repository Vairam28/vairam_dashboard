import React from 'react';
import Box from '@mui/material/Box';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import { useStyles } from "./style";


export const Dropdown = (props) => {

    const classes = useStyles(props);

    const [age, setAge] = React.useState('');

    const handleChange = (event) => {
        setAge(event.target.value);
    };

    return <Box className={classes.root}>
        <FormControl className={classes.dropdownIcon}>
            <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={age}
                onChange={handleChange}
                displayEmpty
                className={classes.dropdown}
                size={'small'}
                sx={{ "& .css-jedpe8-MuiSelect-select-MuiInputBase-input-MuiOutlinedInput-input":{padding:props?.padding}}}
            >
                <MenuItem value="">{props.placeholder}</MenuItem>
                <MenuItem value={10}>In progress</MenuItem>
                <MenuItem value={20}>Adding</MenuItem>
                <MenuItem value={30}>Awaits</MenuItem>
            </Select>
        </FormControl>
    </Box>

}