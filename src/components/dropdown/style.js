import { makeStyles } from "@mui/styles";

export const useStyles = makeStyles((theme, props) => ({

    dropdown: {
        fontSize: "12px",
        "& .css-11u53oe-MuiSelect-select-MuiInputBase-input-MuiOutlinedInput-input.css-11u53oe-MuiSelect-select-MuiInputBase-input-MuiOutlinedInput-input.css-11u53oe-MuiSelect-select-MuiInputBase-input-MuiOutlinedInput-input": {
            padding: "12px 14px",
            fontSize: "12px",
            borderRadius: "10px",
            textAlign: "initial",
            paddingRight: "0px"
        },
        "& .css-11u53oe-MuiSelect-select-MuiInputBase-input-MuiOutlinedInput-input": {
            fontWeight: "bold",
            color: "#5A5A5A"
        },
        "& .css-jedpe8-MuiSelect-select-MuiInputBase-input-MuiOutlinedInput-input.css-jedpe8-MuiSelect-select-MuiInputBase-input-MuiOutlinedInput-input.css-jedpe8-MuiSelect-select-MuiInputBase-input-MuiOutlinedInput-input": {
            paddingRight: "0px",
        },
        "& .MuiSelect-select-MuiInputBase-input-MuiOutlinedInput-input": {
            padding: (props) => props?.padding ?? "10px"
        },

    },
    dropdownIcon: {
        width: "100%",
        "& .css-hfutr2-MuiSvgIcon-root-MuiSelect-icon": {
            color: "rgb(33,33,33)",
        },
        "& .css-1d3z3hw-MuiOutlinedInput-notchedOutline": {
            borderRadius: "10px",
        },
        "& .css-yf8vq0-MuiSelect-nativeInput": {
            position: "relative",
            width:"50%"
        }
    }
}))