import { makeStyles } from "@mui/styles";

export const useStyles = makeStyles((theme) => ({
    root: {
        textAlign: "center",
        marginBottom: "8px",
    },
    headText: {
        fontSize: "13px",
        fontWeight: "bold",
    },
    numberText: {
        fontSize: "12px",
        textAlign: "center",
        fontWeight: "500",
        backgroundColor: "black",
        color: "white",
        borderRadius: "50%",
        padding: "2px",
        width: "1.4rem",
        height: "1.4rem",
    }
}))