import React from 'react';
import { Stack, Typography } from '@mui/material';
import Box from '@mui/material/Box';
import { useStyles } from "./style";


export const HeaderTop = ({
    title={},
    numbers={}
}) => {

    const classes = useStyles();

    return <Box className={classes.root}>
        <Stack direction="row" spacing={1} alignItems="center" >
            <Typography variant="h6" gutterBottom={false} component="div" className={classes.headText}>
                {title}
            </Typography>
            <Box className={classes.numberText}>
                {numbers}
            </Box>
        </Stack>
    </Box>

}