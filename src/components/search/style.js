import { makeStyles } from "@mui/styles";

export const useStyles = makeStyles((theme) => ({
    root: {
        textAlign: "center",
    },
    textField: {
        backgroundColor: "rgb(242,244,246)",
        borderRadius: "10px",
        maxWidth: "255px",
        position: "initial",
        "& .css-1ms0xco-MuiInputBase-root-MuiOutlinedInput-root": {
            borderRadius: "10px"
        },
        "& .css-1t8l2tu-MuiInputBase-input-MuiOutlinedInput-input": {
            padding: "8.5px 54px"
        },
        "& .css-1d3z3hw-MuiOutlinedInput-notchedOutline":{
            border:"none"
        }
    },
    parentSearch: {
        position: "relative",
    },
    searchIcon: {
        position: "absolute",
        top: "11px",
        left: "13px",
        color: "rgb(152,153,155)",
        fontSize: "20px"
    }
}))