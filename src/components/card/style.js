import { makeStyles } from "@mui/styles";

export const useStyles = makeStyles((theme) => ({
    root: {
        border: "1px solid #E4EAEE",
        margin: 0,
        display: "block",
        backgroundColor: "white",
        padding: "10px",
        cursor: "pointer",
    },
    rowTop: {
        display: "flex",
        justifyContent: "space-between",
        paddingBottom: "4px"
    },
    date: {
        color: "#5F5F5F",
        fontSize: "10px",
        fontWeight: "bold",
        marginRight: "7px",
        whiteSpace: "nowrap",
                [theme.breakpoints.down("md")]: {
                    whiteSpace: "initial", 
                },
    },
    time: {
        color: "#5F5F5F",
        fontSize: "10px",
        fontWeight: "bold",
        marginLeft: "0px !important",
        paddingLeft: "7px",
        borderLeft: "1px solid #666666"
    },
    numberText: {
        width: "1.4rem",
        height: " 1.4rem",
        fontSize: "14px",
        textAlign: "center",
        fontWeight: "500",
        color: "white",
        borderRadius: "50%",
        padding: "2px"
    },
    flags: {
        fontSize: "15px",
    },
    hours: {
        width: "60px",
        height: " 1.6rem",
        fontSize: "11px",
        textAlign: "center",
        fontWeight: "bold",
        borderRadius: "4px",
        padding: "4px",
        marginLeft:"10px !important",
        textTransform: "initial",
    },
    description: {
        fontSize: "13px",
        paddingBottom: "10px"
    },
    whiteCard: {
        paddingBottom: "8px"
    },
    features: {
        height: " 1.6rem",
        fontSize: "10px",
        textAlign: "center",
        fontWeight: "bold",
        borderRadius: "4px",
        padding: "5px 9px",
        color: "#676767",
        marginLeft: "7px",
        backgroundColor: "#F1F1F1",
        margin: "0px !important",
        marginRight: "8px !important",
        whiteSpace: "nowrap"
    },
    bottomIcons: {
        display: "flex",
        justifyContent: "space-between",
    },
    spring: {
        color: "#676767",
        fontSize: "16px",
        cursor: "pointer",
        marginLeft: "6px !important",
    },
    progressBar: {
        marginLeft: "6px !important",
        "& .MuiCircularProgress-circle": {
            backgroundColor: "red"
        }
    },
    percent: {
        fontSize: "10px",
        marginLeft: "4px !important",
        marginTop: "3px !important"
    },
    star: {
        fontSize: "18px",
        margintop: "2px",
    },
    parentStar:{
        paddingTop:"0px",
        paddingLeft:"0px",
        paddingRight:"0px",
        marginLeft:"2px !important"
    },
    dots:{
        borderTop: "1px dashed #989898",
        display: "block",
        borderRadius: "0px 0px 10px 10px",
        backgroundColor: "white",
        padding: "10px",
        cursor: "pointer",
        paddingTop:"0px",
        paddingBottom:"0px"
    },
    timerImg:{
        width:"14px",
    },
    parentTimer:{
        paddingRight:"0px"
    }
}))