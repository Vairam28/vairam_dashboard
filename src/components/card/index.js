import React from 'react';
import { Stack, Typography, IconButton, Button } from '@mui/material';
import Box from '@mui/material/Box';
import FlagIcon from '@mui/icons-material/Flag';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import TrackChangesIcon from '@mui/icons-material/TrackChanges';
import TextSnippetIcon from '@mui/icons-material/TextSnippet';
import Tooltip from '@mui/material/Tooltip';
import CircularProgress from '@mui/material/CircularProgress';
import { Dropdown } from '../dropdown';
import StarIcon from '@mui/icons-material/Star';
import { useStyles } from "./style";

export const Card = ({
    spring = "Goals (2)",
    notepad = "tasks (1)",
    val = {},
    padding = "",
}) => {

    const classes = useStyles();

    return <Box>
        <Box className={classes.root} sx={{ borderRadius: val?.borderRadius}}>
            <Box className={classes.rowTop}>
                <Stack direction="row" spacing={1} alignItems="center" >
                    <Typography className={classes.date} gutterBottom={false} >
                        {val?.date}
                    </Typography>
                    <Typography className={classes.time} gutterBottom={false} >
                        {val.time}
                    </Typography>
                    <IconButton className={classes.numberText} sx={{ backgroundColor: val?.backgroundClr }} >
                        <FlagIcon sx={{ color: val?.flag }} className={classes.flags} />
                    </IconButton>
                    {val?.cardButton && <Button sx={{ color: val?.color, border: val?.border, backgroundColor: val?.background }} className={classes.hours}>
                        {val.hours}
                    </Button>}
                    {
                        val?.startButton && <IconButton className={classes.parentStar}>
                            <StarIcon className={classes.star} sx={{ color: val?.star }} />
                        </IconButton>
                    }
                </Stack>
                <IconButton>
                    <MoreVertIcon />
                </IconButton>
            </Box>
            <Typography className={classes.description} gutterBottom={false} >
                {val?.title}
            </Typography>
            <Stack direction="row" spacing={2} alignItems="center" className={classes.whiteCard} >
                <Typography className={classes.features}>
                    {val?.features}
                </Typography>
                <Typography className={classes.features}>
                    {val?.goal}
                </Typography>
                <Typography className={classes.features}>
                    {val?.colabo}
                </Typography>
            </Stack>
            <Box className={classes.bottomIcons}>
                <Stack direction="row" spacing={2} alignItems="center">
                    <Tooltip title={spring} arrow className={classes.tooltp} >
                        <TrackChangesIcon className={classes.spring} />
                    </Tooltip>
                    <Tooltip title={notepad} arrow>
                        <TextSnippetIcon className={classes.spring} />
                    </Tooltip>
                    <CircularProgress variant="determinate" size={13} thickness={6} value={val?.range} color="success" className={classes.progressBar} />
                    <span className={classes.percent}>{val?.percentage}</span>
                </Stack>
                <Box sx={{ width: "117px" }}>
                    <Dropdown placeholder={val?.placeholder} padding={val?.padding}/>
                </Box>
            </Box>
        </Box>
        {
            val?.dottedline && <Box className={classes.dots}>
                <Stack direction="row" spacing={1} alignItems="center" justifyContent={"space-between"} >
                    <Box display="flex" >
                        <Typography className={classes.date} gutterBottom={false} >
                            {val?.scheTo}
                        </Typography>
                        <Typography className={classes.time} gutterBottom={false} >
                            {val.timing}
                        </Typography>
                    </Box>
                    <IconButton className={classes.parentTimer} >
                        <img src="/images/timer.png" alt="timer" className={classes.timerImg} />
                    </IconButton>
                </Stack>
            </Box>
        }
    </Box>
}
