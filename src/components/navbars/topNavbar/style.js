import { makeStyles } from "@mui/styles";

export const useStyles = makeStyles((theme) => ({
    grow: {
        flexGrow: 1,
        position: "fixed",
        zIndex: 1,
        width: "100%",

    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1
    },
    title: {
        display: 'block',
    },
    titleContainer: {
        marginLeft: theme.spacing(2)
    },
    menuIcon: {
        [theme.breakpoints.up("md")]: {
            display: "none"
        }
    },
    textField: {
        width: "190px",

        "& .css-zp3tkw-MuiInputBase-root-MuiOutlinedInput-root": {
            backgroundColor: "white",
            borderRadius: "10px"
        }
    },
    navbar: {
        display: "flex",
        justifyContent: "space-between",
        width: "91%",
        margin: "0 0 0 auto",
        [theme.breakpoints.only("xs")]: {
            width: "100%",
            display: "block",
            paddingBottom:"14px"
        },
        [theme.breakpoints.only("sm")]: {
            width: "100%",
            display: "flex",
        }
    },
    textBox: {
        color: "black",
        fontWeight: "600",
        [theme.breakpoints.down("sm")]: {
            paddingBottom:"8px"
        }
    },
    iconColor: {
        color: "black"
    },
    borderLeft: {
        borderLeft: "1px solid #E4EAEE",
        width: "10px",
        height: "18px",
    },
    toggleBar:{
        backgroundColor:"#F2F4F6"
    },
    sacImg:{
        [theme.breakpoints.down("sm")]: {
            justifyContent:"space-between",
            paddingTop:"8px"
        }
    },
    navDropdown:{
        width:"100px",
        [theme.breakpoints.down("sm")]: {
            width:"200px",
        }
    },
    searchMob:{
        [theme.breakpoints.only("xs")]: {
            width:"187px",
        },
        [theme.breakpoints.only("sm")]: {
            width:"163px",
        },
    }
}))