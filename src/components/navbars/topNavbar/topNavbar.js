import React from 'react';
import { AppBar, Toolbar, IconButton, Typography, Drawer } from '@mui/material';
import { Menu as MenuIcon } from '@mui/icons-material';
import { SideNavBar } from '..';
import Box from '@mui/material/Box';
import { Search } from '../../search';
import { Dropdown } from "../../dropdown";
import CardGiftcardIcon from '@mui/icons-material/CardGiftcard';
import NotificationsNoneOutlinedIcon from '@mui/icons-material/NotificationsNoneOutlined';
import Avatar from '@mui/material/Avatar';
import Stack from '@mui/material/Stack';
import { useStyles } from "./style";

export const TopNavBar = (props) => {

    const classes = useStyles();

    const [state, setState] = React.useState({
        openSideNavBar: false
    })

    const toogleSideNavBar = () => {
        setState({
            ...state,
            openSideNavBar: !state.openSideNavBar
        })
    }

    return (
        <Box className={classes.grow}>
            <AppBar position="static" className={classes.appBar}>
                <Toolbar className={classes.navbar}>
                    {/* togglebar icon */}
                    <IconButton className={classes.menuIcon} onClick={toogleSideNavBar} size="large">
                        <MenuIcon htmlColor="grey" />
                    </IconButton>
                    {/* togglebar icon */}
                    <Box>
                        <Typography className={classes.textBox}>
                            Productivity Framework
                        </Typography>
                    </Box>
                    {/* search */}
                    <Box className={classes.searchMob}>
                        <Search />
                    </Box>
                    {/* search */}
                    <Stack direction="row" spacing={3} alignItems="center" className={classes.sacImg} >
                        {/* dropdown */}
                        <Box className={classes.navDropdown}>
                            <Dropdown placeholder="Add me"/>
                        </Box>
                        {/* dropdown */}
                        <Box className={classes.borderLeft}>
                        </Box>
                        <CardGiftcardIcon className={classes.iconColor} />
                        <NotificationsNoneOutlinedIcon className={classes.iconColor} />
                        {/* avatar */}
                        <Avatar
                            alt="Remy Sharp"
                            src="/images/sachin.webp"
                            sx={{ width: 42, height: 42, borderRadius: "10px" }}
                            className={classes.avataImg}
                        />
                        {/* avatar */}
                    </Stack>
                    <Drawer
                        open={state.openSideNavBar}
                        variant={"temporary"}
                        anchor="left"
                        onClose={toogleSideNavBar}
                    >
                        <Box style={{ width: 70 }}>
                            <SideNavBar isMobile={true} />
                        </Box>
                    </Drawer>
                </Toolbar>
            </AppBar>
        </Box>
    );
}
