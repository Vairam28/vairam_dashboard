import { makeStyles } from "@mui/styles";
const drawerWidth = 56;
export const useStyles = makeStyles((theme) => ({
    root: {
        width: props => props?.isMobile ? 70 : drawerWidth,
        position: 'absolute',

    },
    drawer: {
        // height: props => props?.isMobile ? `100vh` : `calc(100vh - 64px)`,
        // width: props => props?.isMobile ? 70 : drawerWidth,
        backgroundColor: "#11174C",
        height: "100vh",
        width: "70px",
        position: "fixed",
        top: "0",
        zIndex: "1",
    },
    drawerContainer: {
        overflow: 'hidden',
    },
    logo: {
        color: "#C74F7A"
    },
    borderAround: {
        padding: "5px",
        marginTop: "10px",
        borderRadius: "8px",
        backgroundColor: "#333069",
        color: "white",
        margin: "auto",
        textAlign: "center",
    },
    subTitle: {
        color: "white",
        fontSize: "10px",
        textAlign: "center",
        paddingTop: "8px"
    },
    convasBorder: {
        padding: "5px",
        marginTop: "10px",
        borderRadius: "8px",
        backgroundColor: "#63B9A8",
        color: "white",
        margin: "auto",
        textAlign: "center",
    },
    iconSize: {
        display: "flex",
        textAlign: "center",
        justifyContent: "center",
        marginLeft: "2px"
    },
    settingMob:{
        
        [theme.breakpoints.only("xs")]: 
        {
            marginTop:"13px !important"

        },
        [theme.breakpoints.only("sm")]: 
        {
            marginTop:"16px !important"

        }
    }
}))