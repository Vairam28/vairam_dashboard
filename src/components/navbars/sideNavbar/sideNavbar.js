import React from 'react';
import { Paper, Typography, Stack } from '@mui/material';
import AllInclusiveIcon from '@mui/icons-material/AllInclusive';
import LocalFireDepartmentOutlinedIcon from '@mui/icons-material/LocalFireDepartmentOutlined';
import Box from '@mui/material/Box';
import ArticleOutlinedIcon from '@mui/icons-material/ArticleOutlined';
import RadarOutlinedIcon from '@mui/icons-material/RadarOutlined';
import AodIcon from '@mui/icons-material/Aod';
import SettingsOutlinedIcon from '@mui/icons-material/SettingsOutlined';
import { useStyles } from "./style";

export const SideNavBar = (props) => {

    const classes = useStyles(props);


    return (
        <div className={classes.root}>
            <Paper
                className={classes.drawer}
                square
            >
                <div className={classes.drawerContainer}>
                </div>
                <Stack direction="column" spacing={14} alignItems="center" sx={{paddingTop:"27px"}}>
                    <Stack direction="column" spacing={2} alignItems="center">
                        <AllInclusiveIcon className={classes.logo} />
                        <Box>
                            <Box className={classes.borderAround}>
                                <LocalFireDepartmentOutlinedIcon className={classes.iconSize} />
                            </Box>
                            <Typography gutterBottom={false} className={classes.subTitle} >
                                Actions
                            </Typography>
                        </Box>
                        <Box>
                            <Box className={classes.borderAround}>
                                <RadarOutlinedIcon className={classes.iconSize} />
                            </Box>
                            <Typography gutterBottom={false} className={classes.subTitle} >
                                Goals
                            </Typography>
                        </Box>
                        <Box>
                            <Box className={classes.borderAround}>
                                <ArticleOutlinedIcon className={classes.iconSize} />
                            </Box>
                            <Typography gutterBottom={false} className={classes.subTitle} >
                                Docs
                            </Typography>
                        </Box>
                        <Box>
                            <Box className={classes.convasBorder}>
                                <AodIcon className={classes.iconSize} />
                            </Box>
                            <Typography gutterBottom={false} className={classes.subTitle} >
                                Canvas
                            </Typography>
                        </Box>
                    </Stack>
                    {/* settings */}
                    <Stack direction="column" spacing={2} alignItems="center" className={classes.settingMob}>
                        <Box>
                            <Box className={classes.borderAround}>
                                <SettingsOutlinedIcon className={classes.iconSize} />
                            </Box>
                            <Typography gutterBottom={false} className={classes.subTitle} >
                                Settings
                            </Typography>
                        </Box>
                    </Stack>
                    {/* settings */}
                </Stack>
            </Paper>
        </div>
    );
}
