import { makeStyles } from "@mui/styles";

export const useStyles = makeStyles((theme) => ({
    root: {
        marginTop: "2%",
        paddingTop: "55px",
        backgroundColor:"#F2F4F6",
        boxShadow: "0px 2px 1px -1px rgb(0 0 0 / 20%), 0px 1px 1px 0px rgb(0 0 0 / 14%), 0px 1px 3px 0px rgb(0 0 0 / 12%)",
        [theme.breakpoints.down("sm")]: {
            paddingTop:"185px"
        }
    },
    filterBox: {
        border: "1px solid #E4EAEE",
        padding: "7px",
        marginLeft: "4px",
        borderRadius: "8px",
        textAlign: "center",
        display: "flex",
        justifyContent: "center",

    },
    searchRow:{
        marginBottom: "12px",
    },
    dropDownRow:{
        marginBottom: "12px",  
    },
    cardsParent:{
        height: "337px",
        marginTop: "6px",
        padding: "8px",
        borderRadius: "14px",
        backgroundColor: "rgb(255, 244, 235)"
    },
    cardSchedule:{
        backgroundColor:"rgb(235, 237, 245);",
        height: "337px",
        marginTop: "6px",
        padding: "8px",
        borderRadius: "14px",
    },
    delegate:{
        backgroundColor:"rgb(228, 245, 239);",
        height: "337px",
        marginTop: "6px",
        padding: "8px",
        borderRadius: "14px",
    },
    dont:{
        backgroundColor:" rgb(252, 242, 243);",
        height: "337px",
        marginTop: "6px",
        padding: "8px",
        borderRadius: "14px",

    },
    topTitle:{
        paddingLeft: "12px"
    },
    highlights:{
        backgroundColor:"#e8f0da",
        height: "688px",
        marginTop: "6px",
        padding: "8px",
        borderRadius: "14px",
        border: "1px solid #F8FBF3",
        marginRight:"6px"
    },
    parentCard:{
        backgroundColor:"#F2F4F6",
        boxShadow: "0px 2px 1px -1px rgb(0 0 0 / 20%), 0px 1px 1px 0px rgb(0 0 0 / 14%), 0px 1px 3px 0px rgb(0 0 0 / 12%)"
    },
    leftCardParent:{
        backgroundColor:"white",
        borderRadius:"0px 0px 10px 0px",
    },
    rightCardParent:{
        backgroundColor:"white",
        borderRadius:"10px",
        boxShadow: "0px 2px 1px -1px rgb(0 0 0 / 20%), 0px 1px 1px 0px rgb(0 0 0 / 14%), 0px 1px 3px 0px rgb(0 0 0 / 12%)",
        [theme.breakpoints.only("lg")]: {
            paddingRight:"0px",
            paddingLeft:"0px"
        }
    }
}))