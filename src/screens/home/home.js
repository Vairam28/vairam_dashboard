import React from 'react';
import { Stack, Box } from '@mui/material';
import Grid from '@mui/material/Grid';
import FilterAltOutlinedIcon from '@mui/icons-material/FilterAltOutlined';
import { Card, HeaderTop, Search, Dropdown } from "../../components"
import { useStyles } from "./style";

export const Home = props => {

    const classes = useStyles();


    var cardMapping = [
        {
            date: "sep 28 2021",
            time: "6.30pm",
            flag: "red",
            hours: "Overdue",
            color: "#E5513C",
            border: "1px solid #E5513C",
            background: "#FCF1F0",
            title: "Coordinate with development team for assume module",
            features: "5157-feature",
            goal: "Goal",
            colabo: "#Colabo",
            percentage: "30%",
            star: "red",
            backgroundClr: "rgb(252, 241, 240)",
            cardButton: true,
            startButton: true,
            range: 30,
            borderRadius:"10px",
            placeholder:"Started",
            padding:"4px 10px",
        },
        {
            date: "nov 28 2021",
            time: "9.10pm",
            flag: "orange",
            hours: "3 hours",
            color: "#EF9B12",
            border: "1px solid #EF9B12",
            background: "#FEF7EC",
            title: "Coordinate with development team for assume module",
            features: "5157-feature",
            goal: "Goal",
            colabo: "#Colabo",
            percentage: "45%",
            range: 45,
            star: "#EF9C14",
            backgroundClr: "rgb(252, 241, 240);",
            cardButton: true,
            startButton: true,
            borderRadius:"10px",
            placeholder:"Pending",
            padding:"4px 10px"

        },
        {
            date: "dec 18 2021",
            time: "1.10pm",
            flag: "orange",
            hours: "3 hours",
            color: "#E5513C",
            border: "1px solid #E5513C",
            background: "#FCF1F0",
            title: "Coordinate with development team for assume module",
            features: "5157-feature",
            goal: "Goal",
            colabo: "#Colabo",
            percentage: "60%",
            range: 60,
            star: "#EF9C14",
            backgroundClr: "rgb(252, 241, 240)",
            cardButton: false,
            startButton: false,
            borderRadius:"10px",
            placeholder:"Pending",
            padding:"4px 10px"
        }

    ]

    var toDoList = [
        {
            date: "jun 21 2021",
            time: "10.00pm",
            flag: "red",
            hours: "Overdue",
            color: "#E5513C",
            border: "1px solid #E5513C",
            background: "#FCF1F0",
            title: "Completion of productivity framework design on or before july",
            features: "5157-feature",
            goal: "Goal",
            colabo: "#Colabo",
            percentage: "74%",
            range: 74,
            star: "#EF9C14",
            backgroundClr: "rgb(252, 241, 240);",
            cardButton: true,
            startButton: false,
            borderRadius:"10px",
            placeholder:"In develop",
            padding:"4px 10px"
        }

    ]
    var delegate = [
        {
            date: "nov 28 2011",
            time: "7.00pm",
            flag: "orange",
            hours: "2 hours",
            color: "#EF9B12",
            border: "1px solid #EF9B12",
            background: "#FEF7EC",
            title: "coordinate with development team for assume module",
            features: "5157-feature",
            goal: "Goal",
            colabo: "#Colabo",
            percentage: "91%",
            range: 91,
            star: "#EF9C14",
            backgroundClr: "rgb(252, 241, 240);",
            cardButton: false,
            startButton: false,
            borderRadius:"10px",
            placeholder:"Pending",
            padding:"4px 10px"
        }

    ]
    var schedule = [
        {
            date: "mar 18 2021",
            time: "9.00pm",
            flag: "orange",
            hours: "2 hours",
            color: "#EF9B12",
            border: "1px solid #EF9B12",
            background: "#FEF7EC",
            title: "coordinate with development team for assume module",
            features: "5157-feature",
            goal: "Goal",
            colabo: "#Colabo",
            percentage: "81%",
            range: 81,
            star: "#EF9C14",
            backgroundClr: "rgb(252, 241, 240);",
            cardButton: true,
            startButton: false,
            dottedline: true,
            scheTo:"schedule to jun 30,2021",
            timing:"12.00pm",
            borderRadius:"10px 10px 0px 0px",
            placeholder:"In progress",
            padding:"4px 10px"
        }

    ]

    var highlights = [
        {
            date: "jul 11 2020",
            time: "1.00pm",
            flag: "red",
            hours: "Overdue",
            color: "#E5513C",
            border: "1px solid #E5513C",
            background: "#FCF1F0",
            title: "Collabrate with health circle DEV team fror UI feedbacks",
            features: "5157-feature",
            goal: "Goal",
            colabo: "#Colabo",
            percentage: "54%",
            range: 54,
            star: "#EF9C14",
            backgroundClr: "rgb(252, 241, 240);",
            cardButton: true,
            startButton: false,
            borderRadius:"10px",
            placeholder:"Pending",
            padding:"4px 10px"
        },
        {
            date: "aug 01 2019",
            time: "11.00pm",
            flag: "orange",
            hours: "2 hours",
            color: "#EF9B12",
            border: "1px solid #EF9B12",
            background: "#FEF7EC",
            title: "Forward TanyaCare Web design requirments to natha",
            features: "5157-feature",
            goal: "Goal",
            colabo: "#Colabo",
            percentage: "90%",
            range: 90,
            star: "#EF9C14",
            backgroundClr: "rgb(252, 241, 240);",
            cardButton: false,
            startButton: false,
            borderRadius:"10px",
            placeholder:"In progress",
            padding:"4px 10px"
        }
    ]

    return <Box className={classes.root}>
        <Grid container spacing={2}>
            {/* left side card */}
            <Grid item xs={12} sm={12} md={12} lg={3.2} className={classes.leftCardParent}>
                <Box p={1}>
                    {/* title */}
                    <HeaderTop title="My Bucket" numbers="16" />
                    {/* title */}
                    {/* search with filter icon */}
                    <Stack direction="row" spacing={2} alignItems="center" className={classes.searchRow}>
                        <Search />
                        <FilterAltOutlinedIcon />
                    </Stack>
                    {/* search with filter icon */}
                    {/* dropdown components */}
                    <Stack direction="row" spacing={1} alignItems="center" className={classes.dropDownRow}>
                        <Dropdown placeholder="Today" />
                        <Dropdown placeholder="To Me" />
                        <Dropdown placeholder="Open" />
                    </Stack>
                    {/* dropdown components */}
                    <Grid container spacing={2}>
                        {cardMapping?.map((val) => {
                            return (
                                <Grid item xs={12} sm={4} md={4} lg={12}>
                                    <Card val={val} />
                                </Grid>
                            )
                        })
                        }
                    </Grid>
                </Box>
            </Grid >
            {/* left side card */}
            {/* right side card */}
            <Grid item xs={12} sm={12} md={12} lg={8.8} sx={{paddingTop:"16px !important",paddingBottom:"10px"}}>
                <Grid container spacing={1} p={1} pt={0} className={classes.rightCardParent}>
                    <Grid item xs={12} sm={8} md={8} lg={8}>
                        <Grid container spacing={1}>
                            <Grid item xs={12} sm={6} md={6} lg={6}>
                                <Stack spacing={1}>
                                    <Box className={classes.cardsParent}>
                                        <Box p={0.5}>
                                            <Box className={classes.topTitle}>
                                                <HeaderTop title="Do First" numbers="02" />
                                            </Box>
                                            {toDoList?.map((val) => {
                                                return (
                                                    <Card val={val} />
                                                )
                                            })
                                            }
                                        </Box>
                                    </Box>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} sm={6} md={6} lg={6}>
                                <Stack spacing={1}>
                                    <Box className={classes.cardSchedule}>
                                        <Box p={0.5}>
                                            <Box className={classes.topTitle}>
                                                <HeaderTop title="Schedule" numbers="01" />
                                            </Box>
                                            {schedule?.map((val) => {
                                                return (
                                                    <Card val={val} />
                                                )
                                            })
                                            }
                                        </Box>
                                    </Box>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} sm={6} md={6} lg={6}>
                                <Stack spacing={1}>
                                    <Box className={classes.delegate}>
                                        <Box p={0.5}>
                                            <Box className={classes.topTitle}>
                                                <HeaderTop title="Delegate" numbers="01" />
                                            </Box>
                                            {delegate?.map((val) => {
                                                return (
                                                    <Card val={val} />
                                                )
                                            })
                                            }
                                        </Box>
                                    </Box>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} sm={6} md={6} lg={6}>
                                <Stack spacing={1}>
                                    <Box className={classes.dont}>
                                        <Box p={0.5}>
                                            <Box className={classes.topTitle}>
                                                <HeaderTop title="Don't" numbers="0" />
                                            </Box>
                                            {/* {toDoList?.map((val) => {
                                                return (
                                                    <Card val={val} />
                                                )
                                            })
                                            } */}
                                        </Box>
                                    </Box>
                                </Stack>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={12} sm={4} md={4} lg={4}>
                        <Stack spacing={1}>
                            <Box className={classes.highlights}>
                                <Box p={0.5}>
                                    <Box className={classes.topTitle}>
                                        <HeaderTop title="Highlights" numbers="02" />
                                    </Box>
                                    <Grid container spacing={2}>
                                        {highlights?.map((val) => {
                                            return (
                                                <Grid item xs={12} sm={12} md={12} lg={12}>
                                                    <Card val={val} />
                                                </Grid>
                                            )
                                        })
                                        }
                                    </Grid>
                                </Box>
                            </Box>
                        </Stack>
                    </Grid>
                </Grid>
            </Grid>
            {/* right side card */}
        </Grid >
    </Box >
}